<?php

namespace App\Http\Controllers;

use App\Models\Anuncio;
use App\Models\Empresa;
use App\Models\Servicio;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class AnuncioController extends Controller
{

    public function index()
    {
        
        $anuncios = DB::table('anuncios')->orderby('id', 'DESC')
        ->select('anuncios.*','servicios.servicio','empresas.empresa')
        ->leftJoin('servicios','servicios.id','=','anuncios.servicio_id')
        ->leftJoin('empresas','empresas.id','=','anuncios.empresa_id')
        ->get();

        return view('admin.anuncios.index', compact('anuncios'));
    }

    public function create()
    {
        $servicios = Servicio::where('estado', 1)->orderBy('servicio', 'ASC')->get();
        $empresas = Empresa::get();

        return view('admin.anuncios.crear', compact('servicios','empresas'));
    }

    public function store(Request $request)
    {

        if (request()->ajax()) {
            try {
                $anuncio = new Anuncio();
                $anuncio->titulo = Str::upper($request->titulo);
                $anuncio->anuncio = $request->anuncio;
                $anuncio->empresa_id = $request->empresa_id;
                $anuncio->servicio_id = $request->servicio_id;
                $anuncio->salario = $request->salario;
                $anuncio->horario = $request->horario;
                $anuncio->direccion = $request->direccion;
                $anuncio->nota = $request->nota;
                $anuncio->estado = '1';
                $anuncio->save();
                return response()->json([
                    'ok' => 'Se registró con éxito'
                ]);
            } catch (\Throwable $th) {
                throw $th;
            }
        }
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        // dd($id);
        $servicios = Servicio::where('estado', 1)->orderBy('servicio', 'ASC')->get();
        $empresas = Empresa::get();

        $anuncio = DB::table('anuncios')->orderby('id', 'DESC')
        ->select('anuncios.*','servicios.servicio','empresas.empresa')
        ->leftJoin('servicios','servicios.id','=','anuncios.servicio_id')
        ->leftJoin('empresas','empresas.id','=','anuncios.empresa_id')
        ->where('anuncios.id', $id)
        ->first();
        // dd($anuncio);

        return view('admin.anuncios.editar', compact('anuncio','servicios','empresas'));
    }

    public function update(Request $request)
    {
        // dd($request->all());
        if (request()->ajax()) {
            try {
                $anuncio = Anuncio::where('id', $request->id)->first();
                $anuncio->titulo = Str::upper($request->titulo);
                $anuncio->anuncio = $request->anuncio;
                $anuncio->salario = $request->salario;
                $anuncio->horario = $request->horario;
                $anuncio->direccion = $request->direccion;
                $anuncio->nota = $request->nota;
                $anuncio->estado = $request->estado;
                $anuncio->save();
                return response()->json([
                    'ok' => 'Se actualizó con éxito'
                ]);
            } catch (\Throwable $th) {
                throw $th;
            }
        }
    }


    public function destroy($id)
    {
        //
    }
}
