<?php

namespace App\Http\Controllers;

use App\Models\Persona;
use Illuminate\Http\Request;

class RecetaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $personas = Persona::get();
        return view('recetas.index', compact('personas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('recetas.crear');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        if (request()->ajax()) {
            // try {

                $receta = new Persona();
                $receta->nombres = $request->nombres;
                $receta->celular = $request->celular;
                $receta->pais = $request->pais;
                $receta->save();
        
                if ($request->receta) {
                    $urltemp = 'recetas/' . time() . '_0.' . $request->receta->getClientOriginalExtension();
                    // $img = \Image::make($request->receta)->widen(900, function ($constraint) {
                    //     $constraint->upsize();
                    // })->save('img/' . $urltemp);

                    $img = \Image::make($request->receta)->widen(900, function ($constraint) {
                        $constraint->upsize();
                    })->rotate(-90);
                    // $img = \Image::make($request->receta)->resize(900, 500)->flip('v');
                    // $img->flip('v');
                    // $img->rotate(-90);
                    $img->save('img/' . $urltemp);

                    $receta->receta = $urltemp;
                    $receta->save();
                }

                return response()->json([
                    'ok' => 'Se registró con éxito'
                ]);
            // } catch (\Throwable $th) {
            //     throw $th;
            // }
        }


        // return redirect('receta/listar');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
