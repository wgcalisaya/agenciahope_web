<?php

namespace App\Http\Controllers;

use App\Models\Modalidad;
use App\Models\Persona;
use App\Models\Religion;
use App\Models\Servicio;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class PersonaController extends Controller
{

    public function index()
    {
        // $personas = Persona::get();
        $personas = DB::table('personas')
        ->select('personas.*','religions.religion','servicios.servicio','modalidads.modalidad')
        ->leftJoin('religions', 'religions.id', '=', 'personas.religion_id')
        ->leftJoin('servicios', 'servicios.id', '=', 'personas.servicio_id')
        ->leftJoin('modalidads', 'modalidads.id', '=', 'personas.modalidad_id')
        ->orderBy('personas.id','DESC')
        ->get();

        return view('personas.index', compact('personas'));
    }

    public function create()
    {
        $religions = Religion::where('estado', 1)->get();
        $servicios = Servicio::where('estado', 1)->orderBy('servicio', 'ASC')->get();
        $modalidads = Modalidad::where('estado', 1)->get();

        return view('personas.crear', compact('religions','servicios','modalidads'));
    }


    public function store(Request $request)
    {
        if (request()->ajax()) {
            try {
                $receta = new Persona();
                $receta->nombres = Str::upper($request->nombres);
                $receta->apellidos = Str::upper($request->apellidos);
                $receta->edad = $request->edad;
                $receta->celular = $request->celular;
                $receta->religion_id = $request->religion_id;
                $receta->servicio_id = $request->servicio_id;
                $receta->modalidad_id = $request->modalidad_id;
                $receta->ciudad = $request->ciudad;
                $receta->comentario = $request->comentario;
                $receta->estado = 1;
                $receta->save();

                return response()->json([
                    'ok' => 'Se registró con éxito'
                ]);
            } catch (\Throwable $th) {
                throw $th;
            }
        }
    }

    public function registrar(Request $request)
    {
        // dd($request->all());
        if (request()->ajax()) {
            try {
                $receta = new Persona();
                $receta->nombres = Str::upper($request->nombres);
                $receta->apellidos = Str::upper($request->apellidos);
                $receta->edad = $request->edad;
                $receta->celular = $request->celular;
                $receta->religion_id = $request->religion_id;
                $receta->servicio_id = $request->servicio_id;
                $receta->modalidad_id = $request->modalidad_id;
                $receta->ciudad = $request->ciudad;
                $receta->comentario = $request->comentario;
                $receta->estado = 1;
                $receta->save();

                return response()->json([
                    'ok' => 'Se registró con éxito'
                ]);
            } catch (\Throwable $th) {
                throw $th;
            }
        }
    }

    public function registrarVue(Request $request)
    {
        // dd($request->all());
        if (request()->ajax()) {
            try {
                $receta = new Persona();
                $receta->nombres = Str::upper($request->nombres);
                $receta->apellidos = Str::upper($request->apellidos);
                $receta->edad = $request->edad;
                $receta->celular = $request->celular;
                $receta->religion_id = $request->religion;
                $receta->servicio_id = $request->servicio;
                $receta->modalidad_id = $request->modalidad;
                $receta->ciudad = $request->ciudad;
                $receta->comentario = $request->comentario;
                $receta->estado = 1;
                $receta->tipo = $request->tipo;
                if ($request->tipo == 'E') {
                    $receta->tipo = $request->tipo;
                    $receta->direccion = $request->direccion;
                    $receta->solicito = $request->solicito;
                }
                $receta->save();

                return response()->json([
                    'status' => '1',
                    'ok' => 'Se registró con éxito'
                ]);
            } catch (\Throwable $th) {
                throw $th;
            }
        }
    }


    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        // dd($id);

        $persona = DB::table('personas')
        ->where('id', $id)
        ->first();
        // dd($persona);

        return view('personas.editar', compact('persona'));
    }

    public function update(Request $request)
    {
        // dd($request->all());
        if (request()->ajax()) {
            try {
                $persona = Persona::where('id', $request->id)->first();
                // $persona->titulo = Str::upper($request->titulo);
                // $persona->anuncio = $request->anuncio;
                // $persona->salario = $request->salario;
                // $persona->horario = $request->horario;
                // $persona->direccion = $request->direccion;
                $persona->observaciones = $request->observaciones;
                $persona->estado = $request->estado;
                $persona->save();
                return response()->json([
                    'ok' => 'Se actualizó con éxito'
                ]);
            } catch (\Throwable $th) {
                throw $th;
            }
        }
    }

    public function destroy($id)
    {
        //
    }
}
