<?php

namespace App\Http\Controllers;

use App\Models\Anuncio;
use App\Models\Modalidad;
use App\Models\Persona;
use App\Models\Religion;
use App\Models\Servicio;
use Illuminate\Http\Request;

class IndexController extends Controller
{

    public function index()
    {
        // $religions = Religion::where('estado', 1)->get();
        // $servicios = Servicio::where('estado', 1)->orderBy('servicio', 'ASC')->get();
        // $modalidads = Modalidad::where('estado', 1)->get();
        // $anuncios = Anuncio::where('estado', 1)->orderby('updated_at', 'DESC')->get();
        // return view('index', compact('religions','servicios','modalidads','anuncios'));
        return view('web.inicio');
    }
    public function contatanos()
    {
        return view('web.contactanos');
    }

    public function empleos()
    {
        $anuncios = Anuncio::where('estado', 1)->orderby('updated_at', 'DESC')->get();
        $anunciosinactivos = Anuncio::where('estado', 0)->orderby('updated_at', 'DESC')->get();
        return view('web.empleos', compact('anuncios','anunciosinactivos'));
    }
    public function servicios()
    {
        $servicios = Servicio::where('estado', 1)->orderBy('servicio','asc')->get();
        return view('web.servicios', compact('servicios'));
    }

    public function indexApi()
    {
        $religions = Religion::where('estado', 1)->get();
        $servicios = Servicio::where('estado', 1)->orderBy('servicio', 'ASC')->get();
        $modalidads = Modalidad::where('estado', 1)->get();

        // return view('index', compact('religions','servicios','modalidads'));
        return response()->json(['religions'=>$religions, 'servicios'=>$servicios, 'modalidades'=>$modalidads]);
    }

    public function apiPersonas()
    {
        $personas = Persona::select('id','created_at','edad','ciudad')->get();

        return response()->json([
            "status" => 1,
            'personas'=>$personas]
        );
    }
}
