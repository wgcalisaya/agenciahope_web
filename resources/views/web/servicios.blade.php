@extends('web.layout')

@section('content')

<!-- Header End -->
<div class="container-xxl py-5 bg-dark page-header-servicios mb-5">
    <div class="container my-5 pt-5 pb-4">
        <h1 class="display-3 text-white mb-3 animated slideInDown">Nuestros servicios</h1>
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb text-uppercase">
                <li class="breadcrumb-item"><a href="/">Inicio</a></li>
                <li class="breadcrumb-item text-white active" aria-current="page">Nuestros servicios</li>
            </ol>
        </nav>
    </div>
</div>
<!-- Header End -->


<!-- Category Start -->
<div class="container-xxl py-5">
    <div class="container">
        <h2 class="text-center mb-5 wow fadeInUp" data-wow-delay="0.1s">Nuestros servicios</h2>
        {{-- <h2 class=" text-center mb-4">Qué hacemos en Agencia HOPE?</h2> --}}
                        <p class="text-center mb-4">Somos una agencia que ofrece el servicio de evaluación y selección del
                            personal calificado para su negocio, empresa u hogar.</p>
        <div class="row g-4">
            @foreach ($servicios as $item)    
            <div class="col-lg-3 col-sm-6 wow fadeInUp" data-wow-delay="0.5s">
                <a class="cat-item rounded p-4" href="#">
                    <img src="{{$item->portada}}" alt="" width="100%">
                    {{-- <i class="fa fa-3x fa-user-tie text-primary mb-4"></i> --}}
                    <h6 class="mb-3">{{$item->servicio}}</h6>
                    {{-- <p class="mb-0">123 Vacancy</p> --}}
                </a>
            </div>
            @endforeach   
        </div>
        <br>
        <div class="text-center tab-pane fade show p-0 active">
            <a class="btn btn-primary py-3 px-5" href="empleos" >Ver lista de Empleos </a>
        </div>
    </div>
</div>
<!-- Category End -->

@endsection