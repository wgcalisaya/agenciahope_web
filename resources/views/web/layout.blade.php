<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="utf-8">
    <title>Agencia de empleos - HOPE</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta content="" name="keywords">
    <meta content="" name="description">

    <!-- Favicon -->
    <link href="{{ asset('templweb/img/favicon.ico') }}" rel="icon">

    <!-- Google Web Fonts -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Heebo:wght@400;500;600&family=Inter:wght@700;800&display=swap"
        rel="stylesheet">

    <!-- Icon Font Stylesheet -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.0/css/all.min.css" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.4.1/font/bootstrap-icons.css" rel="stylesheet">

    <!-- Libraries Stylesheet -->
    <link href="{{ asset('templweb/lib/animate/animate.min.css') }}" rel="stylesheet">
    <link href="{{ asset('templweb/lib/owlcarousel/assets/owl.carousel.min.css') }}" rel="stylesheet">

    <!-- Customized Bootstrap Stylesheet -->
    <link href="{{ asset('templweb/css/bootstrap.min.css') }}" rel="stylesheet">

    <!-- Template Stylesheet -->
    <link href="{{ asset('templweb/css/style-ok.css') }}" rel="stylesheet">
    <!-- Google tag (gtag.js) -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-CTFTNQ4KWV"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'G-CTFTNQ4KWV');
    </script>
</head>

<body>
    <div class="container-xxl bg-white p-0">
        <!-- Spinner Start -->
        {{-- <div id="spinner" class="show bg-white position-fixed translate-middle w-100 vh-100 top-50 start-50 d-flex align-items-center justify-content-center">
            <div class="spinner-border text-primary" style="width: 3rem; height: 3rem;" role="status">
                <span class="sr-only">Loading...</span>
            </div>
        </div> --}}
        <!-- Spinner End -->


        <!-- Navbar Start -->
        <nav class="navbar navbar-expand-lg bg-white navbar-light shadow sticky-top p-0"
            style="background-color: #012452 !important">
            <a href="/" class="navbar-brand d-flex align-items-center text-center py-0 px-4 px-lg-5">
                {{-- <h1 class="m-0 text-white">Agencia Hope</h1> --}}
                <img src="{{ asset('img/recursos/logoah.png') }}" alt="" width="30%">
            </a>
            <button type="button" class="navbar-toggler me-4" data-bs-toggle="collapse"
                data-bs-target="#navbarCollapse">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarCollapse">
                <div class="navbar-nav ms-auto p-4 p-lg-0 ">
                    <a href="/" class="nav-item nav-link text-white">Inicio</a>
                    {{-- <a href="empleos" class="nav-item nav-link text-white">Empleos</a> --}}
                    <a href="servicios" class="nav-item nav-link text-white">Nuestros servicios</a>
                    {{-- <a href="about.html" class="nav-item nav-link text-white">About</a> --}}
                    {{-- <div class="nav-item dropdown">
                        <a href="#" class="nav-link dropdown-toggle" data-bs-toggle="dropdown">Jobs</a>
                        <div class="dropdown-menu rounded-0 m-0">
                            <a href="job-list.html" class="dropdown-item">Job List</a>
                            <a href="job-detail.html" class="dropdown-item">Job Detail</a>
                        </div>
                    </div> --}}
                    {{-- <div class="nav-item dropdown">
                        <a href="#" class="nav-link dropdown-toggle" data-bs-toggle="dropdown">Pages</a>
                        <div class="dropdown-menu rounded-0 m-0">
                            <a href="category.html" class="dropdown-item">Job Category</a>
                            <a href="testimonial.html" class="dropdown-item">Testimonial</a>
                            <a href="404.html" class="dropdown-item">404</a>
                        </div>
                    </div> --}}
                    <a href="contactanos" class="nav-item nav-link text-white">Contáctanos</a>
                </div>
                <a href="contactanos" class="btn btn-primary rounded-0 py-4 px-lg-5 d-none d-lg-block">Registrarme<i
                        class="fa fa-arrow-right ms-3"></i></a>
            </div>
        </nav>
        <!-- Navbar End -->

        @yield('content')



        <!-- Footer Start -->
        <div class="container-fluid bg-dark text-white-50 footer pt-5 mt-5 wow fadeIn" data-wow-delay="0.1s">
            <div class="container py-5">
                <div class="row g-5">
                    <div class="col-lg-4 col-md-6">
                        <h5 class="text-white mb-4">Empresa</h5>
                        <a class="btn btn-link text-white-50" href="#">Sobre nosotros</a>
                        <a class="btn btn-link text-white-50" href="contactanos">Contactanos</a>
                        <a class="btn btn-link text-white-50" href="#">Nuestros servicios</a>
                        {{-- <a class="btn btn-link text-white-50" href="">Privacy Policy</a> --}}
                        {{-- <a class="btn btn-link text-white-50" href="">Terms & Condition</a> --}}
                    </div>
                    {{-- <div class="col-lg-4 col-md-6">
                        <h5 class="text-white mb-4">Quick Links</h5>
                        <a class="btn btn-link text-white-50" href="">About Us</a>
                        <a class="btn btn-link text-white-50" href="">Contact Us</a>
                        <a class="btn btn-link text-white-50" href="">Our Services</a>
                        <a class="btn btn-link text-white-50" href="">Privacy Policy</a>
                        <a class="btn btn-link text-white-50" href="">Terms & Condition</a>
                    </div> --}}
                    <div class="col-lg-4 col-md-6">
                        <h5 class="text-white mb-4">Contact</h5>
                        <p class="mb-2"><i class="fa fa-map-marker-alt me-3"></i>Tacna</p>
                        <p class="mb-2"><i class="fa fa-phone-alt me-3"></i>
                            <a href="https://wa.me/51969987849" target="../">969987849</a> / <a href="https://wa.me/51952633245" target="../">952633245</a> 
                            
                        </p>
                        <p class="mb-2"><i class="fa fa-envelope me-3"></i>agenciahopetacna@gmail.com</p>
                        <div class="d-flex pt-2">
                            {{-- <a class="btn btn-outline-light btn-social" href=""><i
                                    class="fab fa-twitter"></i></a> --}}
                            <a class="btn btn-outline-light btn-social" href="https://www.facebook.com/agenciahope.pe" target="../"><i
                                    class="fab fa-facebook-f"></i></a>
                            {{-- <a class="btn btn-outline-light btn-social" href=""><i
                                    class="fab fa-youtube"></i></a> --}}
                            {{-- <a class="btn btn-outline-light btn-social" href=""><i
                                    class="fab fa-linkedin-in"></i></a> --}}
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6">
                        <h5 class="text-white mb-4">Recibir ofertas laborales</h5>
                        <p>Registra tus datos para recibir ofertas laborales.</p>
                        <div class="position-relative mx-auto" style="max-width: 400px;">
                            {{-- <input class="form-control bg-transparent w-100 py-3 ps-4 pe-5" type="text"
                                placeholder="Your email">
                            <button type="button"
                                class="btn btn-primary py-2 position-absolute top-0 end-0 mt-2 me-2">SignUp</button> --}}

                                <a class="btn btn-primary py-3 px-5 mt-3" href="contactanos">Quiero registrarme</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="copyright">
                    <div class="row">
                        <div class="col-md-6 text-center text-md-start mb-3 mb-md-0">
                            &copy; <a class="border-bottom" href="#">Agencia Hope</a>, All Right Reserved.

                            <!--/*** This template is free as long as you keep the footer author’s credit link/attribution link/backlink. If you'd like to use the template without the footer author’s credit link/attribution link/backlink, you can purchase the Credit Removal License from "https://htmlcodex.com/credit-removal". Thank you for your support. ***/-->
                            Desarrollado por By <a class="border-bottom" href="https://nodelab.net.pe">Nodelab</a>
                        </div>
                        <div class="col-md-6 text-center text-md-end">
                            <div class="footer-menu">
                                <a href="/">Home</a>
                                <a href="#">Ayuda</a>
                                <a href="#">Preguntas Frecuentes</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Footer End -->


        <!-- Back to Top -->
        <a href="#" class="btn btn-lg btn-primary btn-lg-square back-to-top"><i class="bi bi-arrow-up"></i></a>
    </div>

    <!-- JavaScript Libraries -->
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/js/bootstrap.bundle.min.js"></script>
    <script src="{{ asset('templweb/lib/wow/wow.min.js') }}"></script>
    <script src="{{ asset('templweb/lib/easing/easing.min.js') }}"></script>
    <script src="{{ asset('templweb/lib/waypoints/waypoints.min.js') }}"></script>
    <script src="{{ asset('templweb/lib/owlcarousel/owl.carousel.min.js') }}"></script>

    <!-- Template Javascript -->
    <script src="{{ asset('templweb/js/main.js') }}"></script>
    <script src="{{ asset('js/app002.js') }}" defer></script>
</body>

</html>
