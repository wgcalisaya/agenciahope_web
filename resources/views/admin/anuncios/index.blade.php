@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Anuncios
                        <a href="{{ route('crear_anuncios') }} " class="btn btn-success float-right">Registrar Anuncio</a>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-striped" id="myTable" style="width:100%">
                                <thead>
                                    <tr>
                                        <th scope="col">#</th>
                                        <th scope="col">Titulo</th>
                                        <th scope="col">Anuncio</th>
                                        <th scope="col">Tipo</th>
                                        <th scope="col">Empresa</th>
                                        <th scope="col">Salario</th>
                                        <th scope="col">Horario</th>
                                        <th scope="col">Direccion</th>
                                        <th scope="col">Nota</th>
                                        <th scope="col">Opc</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($anuncios as $item)
                                        <tr style=" {{$item->estado == '0' ? 'background-color: rgb(194, 126, 126);':'' }}">
                                            <th scope="row">{{$loop->index+1}}</th>
                                            <td>{{ $item->titulo }}</td>
                                            <td>{{ $item->anuncio }}</td>
                                            <td>{{ $item->servicio }}</td>
                                            <td>{{ $item->empresa }}</td>
                                            <td>{{ $item->salario }}</td>
                                            <td>{{ $item->horario }}</td>
                                            <td>{{ $item->direccion }}</td>
                                            <td>{{ $item->nota }} <br><br>
                                                <footer class="blockquote-footer">
                                                    Publicado el ({{ \Carbon\Carbon::parse($item->created_at)->format('d/m/y h:m') }})
                                                </footer>
                                            </td>
                                            <td>
                                                <a href="{{route('editar_anuncios',$item->id)}}">Editar</a>
                                            </td>
                                        </tr>
                                    @endforeach
    
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
{{-- <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.13.4/js/jquery.dataTables.min.js"></script> --}}

<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.13.4/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.13.4/js/dataTables.bootstrap5.min.js"></script>
<script>
    $(document).ready(function() {
        $('#myTable').DataTable();
    });
</script>


