@extends('layouts.app')

@section('content')
    <link href="{{ asset('css/jquery.loadingModal.min.css') }}" rel="stylesheet">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <section class="content">
                    <div id="app"></div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card bg-light card-default ">
                                <div class="card-header">
                                    <h3 class="card-title">Crear Anuncio</h3>
                                    <div class="card-tools">
                                        <button type="button" class="btn btn-tool" data-card-widget="collapse"
                                            data-toggle="tooltip" title="Collapse">
                                            <i class="fas fa-minus"></i></button>
                                    </div>
                                </div>
                                <form action="{{ route('guardar_anuncios') }}" method="POST" id="anuncio_form_register"
                                    enctype="multipart/form-data">
                                    @csrf
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="text-secondary">Empresa</label>
                                                    <select name="empresa_id" id="" class="form-select">
                                                        <option value="">-Seleccione-</option>
                                                        @foreach ($empresas as $item)
                                                            <option value="{{ $item->id }}">{{ $item->empresa }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>Titulo del anuncio</label>
                                                    <input type="text" class="form-control" name="titulo">
                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <label class="text-secondary">Descripcion del anuncio</label>
                                                    <textarea name="anuncio" id="" cols="30" rows="3" class="form-control" required></textarea>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="text-secondary">Servicio</label>
                                                    <select name="servicio_id" id="" class="form-select"
                                                        required>
                                                        <option value="">-Seleccione-</option>
                                                        @foreach ($servicios as $item)
                                                            <option value="{{ $item->id }}">{{ $item->servicio }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>Salario</label>
                                                    <input type="text" class="form-control" name="salario">
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>Horario</label>
                                                    <input type="text" class="form-control" name="horario">
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>Dirección de trabajo</label>
                                                    <input type="text" class="form-control" name="direccion">
                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <label class="text-secondary">Nota</label>
                                                    <textarea name="nota" id="" cols="30" rows="3" class="form-control"></textarea>
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="card-footer">
                                        <div class="row">
                                            <div class="col-12">
                                                <a href="{{ route('lista_anuncios') }}" class="btn btn-secondary">Ir a la
                                                    lista</a>
                                                <input type="submit" value="Registrar" class="btn btn-success float-right">
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
@endsection

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="{{ asset('js/jquery.loadingModal.min.js') }}"></script>
<script>
    $(document).ready(function() {
        $('#anuncio_form_register').on('submit', function(event) {
            event.preventDefault();
            $('body').loadingModal({
                text: 'Espere...',
                animation: 'threeBounce',
                opacity: '0.7',
            });
            // $('#load-on').css('display','block');
            $.ajax({
                url: "{{ route('guardar_anuncios') }}",
                method: "POST",
                data: new FormData(this),
                dataType: 'JSON',
                contentType: false,
                cache: false,
                processData: false,
                success: function(data) {
                    if (data.ok) {
                        alert(data.ok);
                        location.reload(true);
                        $('body').loadingModal('hide');
                        window.location.href = 'listar';

                    } else {
                        $('body').loadingModal('hide');
                        alert(data.error);
                        location.reload(true);
                    }
                },
                error: function(XMLHttpRequest, textStatus, errorThrown) {
                    alert("error servidor: " + XMLHttpRequest.statusText);
                    // location.reload(true);
                    $('body').loadingModal('hide');
                }
            })
        })
    })
</script>
