@extends('layouts.app')

@section('content')
    <link href="{{ asset('css/jquery.loadingModal.min.css') }}" rel="stylesheet">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <section class="content">
                    <div id="app"></div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card card-default ">
                                <div class="card-header">
                                    <h3 class="card-title">Crear Persona</h3>
                                    <div class="card-tools">
                                        <button type="button" class="btn btn-tool" data-card-widget="collapse"
                                            data-toggle="tooltip" title="Collapse">
                                            <i class="fas fa-minus"></i></button>
                                    </div>
                                </div>
                                <form action="{{ route('guardar_personas') }}" method="POST" id="receta_form_register"
                                    enctype="multipart/form-data">
                                    @csrf
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <!-- text input -->
                                                <div class="form-group">
                                                    <label class="text-secondary">Nombres</label>
                                                    <input type="text" class="form-control" name="nombres"
                                                        autocomplete="off" required>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <!-- text input -->
                                                <div class="form-group">
                                                    <label class="text-secondary">Apellidos</label>
                                                    <input type="text" class="form-control" name="apellidos"
                                                        autocomplete="off" required>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <!-- text input -->
                                                <div class="form-group">
                                                    <label class="text-secondary">Edad</label>
                                                    <input type="number" class="form-control" name="edad" required>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <!-- text input -->
                                                <div class="form-group">
                                                    <label class="text-secondary">Celular</label>
                                                    <input type="number" class="form-control" name="celular" required>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="text-secondary">Religión</label>
                                                    {{-- <input type="text" class="form-control" name="pais"> --}}
                                                    <select name="religion_id" id="" class="form-select"
                                                        required>
                                                        <option value="">-Seleccione-</option>
                                                        @foreach ($religions as $item)
                                                            <option value="{{ $item->id }}">{{ $item->religion }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="text-secondary">Lugar de procedencia</label>
                                                    <input type="text" class="form-control" name="ciudad" required>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="text-secondary">Servicio</label>
                                                    <select name="servicio_id" id="" class="form-select"
                                                        required>
                                                        <option value="">-Seleccione-</option>
                                                        @foreach ($servicios as $item)
                                                            <option value="{{ $item->id }}">{{ $item->servicio }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="text-secondary">Modalidad</label>
                                                    <select name="modalidad_id" id="" class="form-select"
                                                        required>
                                                        <option value="">-Seleccione-</option>
                                                        @foreach ($modalidads as $item)
                                                            <option value="{{ $item->id }}">{{ $item->modalidad }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <label class="text-secondary">Comentario</label>
                                                    {{-- <input type="text" class="form-control" name="comentario" required> --}}
                                                    <textarea name="comentario" id="" cols="30" rows="3" class="form-control"
                                                        placeholder="Coméntanos de ti..." required></textarea>
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="card-footer">
                                        <div class="row">
                                            <div class="col-12">
                                                <a href="{{ route('lista_personas') }}" class="btn btn-secondary">Ir a la
                                                    lista</a>
                                                <input type="submit" value="Registrar" class="btn btn-success float-right">
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
@endsection

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="{{ asset('js/jquery.loadingModal.min.js') }}"></script>
<script>
    $(document).ready(function() {
        $('#receta_form_register').on('submit', function(event) {
            event.preventDefault();
            $('body').loadingModal({
                text: 'Espere, subiendo imagen...',
                animation: 'threeBounce',
                opacity: '0.7',
            });
            // $('#load-on').css('display','block');
            $.ajax({
                url: "{{ route('guardar_personas') }}",
                method: "POST",
                data: new FormData(this),
                dataType: 'JSON',
                contentType: false,
                cache: false,
                processData: false,
                success: function(data) {
                    if (data.ok) {
                        alert(data.ok);
                        // location.reload(true);
                        $('body').loadingModal('hide');
                        window.location.href = 'listar';

                    } else {
                        $('body').loadingModal('hide');
                        alert(data.error);
                        location.reload(true);
                    }
                },
                error: function(XMLHttpRequest, textStatus, errorThrown) {
                    alert("error servidor: " + XMLHttpRequest.statusText);
                    // location.reload(true);
                    $('body').loadingModal('hide');
                }
            })
        })
    })
</script>
