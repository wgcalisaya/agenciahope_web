@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Personas
                        <a href="{{ route('crear_personas') }} " class="btn btn-success float-right">Registrar Persona</a>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-striped" id="myTable" style="width:100%">
                                <thead>
                                    <tr>
                                        <th scope="col">#</th>
                                        <th scope="col">Nombres y Apellidos</th>
                                        <th scope="col">Edad</th>
                                        <th scope="col">Celular</th>
                                        <th scope="col">Lugar de procedencia</th>
                                        <th scope="col">Servicio</th>
                                        <th scope="col">Modalidad</th>
                                        <th scope="col">Religión</th>
                                        <th scope="col">Comentarios</th>
                                        <th scope="col">Opc</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($personas as $item)
                                        <tr style=" {{$item->estado == '0' ? 'background-color: rgb(194, 126, 126);':'' }}">
                                            <th scope="row">{{$loop->index+1}}</th>
                                            <td>{{ $item->nombres }}, {{ $item->apellidos }}</td>
                                            <td>{{ $item->edad }}</td>
                                            <td><a href="https://wa.me/51{{ $item->celular }}?text=Hola {{ $item->nombres }}, Te escribimos de Agencia Hope. Cuentas con disponibilidad para una oferta laborar?" target="../">{{ $item->celular }}</a></td>
                                            <td>{{ $item->ciudad }}</td>
                                            <td>{{ $item->servicio }}</td>
                                            <td>{{ $item->modalidad }}</td>
                                            <td>{{ $item->religion }}</td>
                                            <td>{{ $item->comentario }} <br><br>
                                                <footer class="blockquote-footer">
                                                    ({{ \Carbon\Carbon::parse($item->created_at)->format('d/m/y h:m') }})
                                                </footer>
                                            </td>
                                            <td>
                                                <a href="{{route('editar_personas',$item->id)}}" class="btn btn-warning">Editar</a>
                                            </td>
                                        </tr>
                                    @endforeach
    
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
{{-- <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.13.4/js/jquery.dataTables.min.js"></script> --}}

<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.13.4/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.13.4/js/dataTables.bootstrap5.min.js"></script>
<script>
    $(document).ready(function() {
        $('#myTable').DataTable();
    });
</script>


