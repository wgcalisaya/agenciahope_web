@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    {{-- <div class="card-header">{{ __('Dashboard') }}</div> --}}

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        {{-- {{ __('You are logged in!') }} --}}
                        Bienvenid@
                        <br><br>
                        <ul class="dropdown-menu position-static d-grid gap-1 p-2 rounded-3 mx-0 shadow w-220px">
                            <li><a class="dropdown-item rounded-2" href="{{ route('lista_personas') }}">Lista de Personas</a>
                            </li>
                            <li><a class="dropdown-item rounded-2" href="{{ route('lista_anuncios') }}">Lista de
                                    Anuncios</a></li>
                            <li><a class="dropdown-item rounded-2 disabled" href="#">Lista de Empleadores</a></li>
                            {{-- <li><a class="dropdown-item rounded-2 disabled" href="#">Ventas</a></li>
                        <li><a class="dropdown-item rounded-2 disabled" href="#">Reportes</a></li> --}}
                            {{-- <li><hr class="dropdown-divider"></li> --}}
                        </ul>
                    </div>
                    <iframe width="100%" height="450" src="https://lookerstudio.google.com/embed/reporting/d2303380-2257-4f89-a9f2-eb48978f69a9/page/DPZMD" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </div>
@endsection
