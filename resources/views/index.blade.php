<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <title>Agencia HOPE</title>
    <!-- Favicon-->
    <link rel="icon" type="image/x-icon" href="assets/favicon.ico" />
    <!-- Bootstrap icons-->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css" rel="stylesheet" />
    <!-- Core theme CSS (includes Bootstrap)-->
    <link href="css/styles.css" rel="stylesheet" />

    <!-- Google tag (gtag.js) -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-CTFTNQ4KWV"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'G-CTFTNQ4KWV');
    </script>
</head>

<body class="d-flex flex-column h-100" style="background-color:rgb(27, 22, 81) important;">
    <main class="flex-shrink-0">
        <!-- Navigation-->
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
            <div class="container px-5" style="background-color:rgb(27, 22, 81) important;">
                <a class="navbar-brand" href="/" style="color: rgb(255, 166, 0)"><img
                        src="{{ asset('img/recursos/logoah.png') }}" alt="" width="30%"></a>

                <button class="navbar-toggler" type="button" data-bs-toggle="collapse"
                    data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                    aria-expanded="false" aria-label="Toggle navigation"><span
                        class="navbar-toggler-icon"></span></button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav ms-auto mb-2 mb-lg-0">
                        <li class="nav-item"><a class="nav-link" href="/">Inicio</a></li>
                        {{-- <li class="nav-item"><a class="nav-link" href="about.html">About</a></li>
                            <li class="nav-item"><a class="nav-link" href="contact.html">Contact</a></li>
                            <li class="nav-item"><a class="nav-link" href="pricing.html">Pricing</a></li>
                            <li class="nav-item"><a class="nav-link" href="faq.html">FAQ</a></li> --}}
                        {{-- <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" id="navbarDropdownBlog" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">Blog</a>
                                <ul class="dropdown-menu dropdown-menu-end" aria-labelledby="navbarDropdownBlog">
                                    <li><a class="dropdown-item" href="blog-home.html">Blog Home</a></li>
                                    <li><a class="dropdown-item" href="blog-post.html">Blog Post</a></li>
                                </ul>
                            </li> --}}
                        {{-- <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" id="navbarDropdownPortfolio" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">Portfolio</a>
                                <ul class="dropdown-menu dropdown-menu-end" aria-labelledby="navbarDropdownPortfolio">
                                    <li><a class="dropdown-item" href="portfolio-overview.html">Portfolio Overview</a></li>
                                    <li><a class="dropdown-item" href="portfolio-item.html">Portfolio Item</a></li>
                                </ul>
                            </li> --}}
                    </ul>
                </div>
            </div>
        </nav>
        <!-- Header-->
        <header class="bg-dark py-5">
            <div class="container px-5">
                <div class="row gx-5 align-items-center justify-content-center">
                    <div class="col-xl-6 col-xxl-6 d-xl-block text-center"><img class="img-fluid rounded-3"
                            src="{{ asset('img/recursos/portada_ahope.png') }}" {{-- src="https://paginasdeempleos.com/wp-content/uploads/2020/02/Paginas-de-empleos.jpg" --}} alt="..." />
                    </div>
                    <div class="col-lg-6 col-xl-7 col-xxl-6">
                        <div class="my-5 text-center text-xl-start">
                            <h1 class="display-5 fw-bolder text-white mb-2">Agencia de empleos Tacna</h1>
                            <p class="lead fw-normal text-white-50 mb-4">Somos una agencia que se encarga de contratar y
                                seleccionar personal para los empleadores</p>
                            <div class="d-grid gap-3 d-sm-flex justify-content-sm-center justify-content-xl-start">
                                <a class="btn btn-primary btn-lg px-4 me-sm-3" href="#features">Registrarme para recibir
                                    ofertas laborales</a>
                                {{-- <a class="btn btn-outline-light btn-lg px-4" href="#!">Nuestros empleadores</a> --}}
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </header>
        <!-- Features section-->
        
        <section class="bg-light rounded-3 py-5 px-1 px-md-5 mb-5" id="features">
            <div class="container px-1 my-5">
                <div class="row gx-5 justify-content-center">
                    <div class="col-lg-5 mb-5 mb-lg-0">
                        <h2 class="fw-bolder mb-0">COMUNÍCATE CON NOSOTROS</h2>
                        <div class="card border-0 bg-light mt-xl-5">
                            <div class="card-body p-4 py-lg-5">
                                <div class="d-flex align-items-center justify-content-center">
                                    <div class="text-center">
                                        {{-- <div class="h6 fw-bolder">Tienes dudas? contáctanos</div> --}}
                                        <p class="text-muted mb-4">
                                            Celular o Wsp: <a href="https://wa.me/51969987849"
                                                target="../">969987849</a>
                                            <br>
                                            {{-- Escribenos al: <a href="#!">info@agenciahope.com</a> --}}
                                        </p>
                                        <div class="h6 fw-bolder">Síguenos en</div>
                                        <a class="fs-5 px-2 link-dark" href="https://www.facebook.com/agenciahope.pe"
                                            target="../"><i class="bi-facebook"></i>/AgenciaHopeTacna</a>
                                        {{-- <a class="fs-5 px-2 link-dark" href="#!"><i class="bi-youtube"></i></a> --}}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-7 col-xl-6">

                        <div class="card shadow p-3 mb-5 bg-body rounded bg-light">
                            <h3 class="card-title">REGISTRATE AQUI</h3>
                            <div class="card-header">
                                <label for="" class="text-secondary">Registra tus datos en el formulario y nos
                                    contactarémos contigo cuando haya una plaza para tu perfil.</label>

                            </div>
                            <form action="{{ route('guardar_personas_web') }}" method="POST" id="receta_form_register"
                                enctype="multipart/form-data">
                                @csrf
                                <div class="card-body bg-light">
                                    <div class="row">
                                        {{-- <div class="col-sm-6">
                                            <div class="form-group">
                                                <label class="text-secondary">Persona / Empleador</label>
                                                <select name="" id="" class="form-select">
                                                    <option value="">-Seleccione tipo-</option>
                                                    <option value="">Busco Trabajo</option>
                                                    <option value="">Soy Empleador</option>
                                                </select>
                                            </div>
                                        </div> --}}
                                        <div class="col-sm-6">
                                            <!-- text input -->
                                            <div class="form-group">
                                                <label class="text-secondary">Nombres</label>
                                                <input type="text" class="form-control" name="nombres"
                                                    autocomplete="off" required>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <!-- text input -->
                                            <div class="form-group">
                                                <label class="text-secondary">Apellidos</label>
                                                <input type="text" class="form-control" name="apellidos"
                                                    autocomplete="off" required>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <!-- text input -->
                                            <div class="form-group">
                                                <label class="text-secondary">Edad</label>
                                                <input type="number" class="form-control" name="edad" required>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <!-- text input -->
                                            <div class="form-group">
                                                <label class="text-secondary">Celular</label>
                                                <input type="number" class="form-control" name="celular" required>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label class="text-secondary">Religión</label>
                                                {{-- <input type="text" class="form-control" name="pais"> --}}
                                                <select name="religion_id" id="" class="form-select"
                                                    required>
                                                    <option value="">-Seleccione-</option>
                                                    @foreach ($religions as $item)
                                                        <option value="{{ $item->id }}">{{ $item->religion }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label class="text-secondary">Lugar de procedencia</label>
                                                <input type="text" class="form-control" name="ciudad" required>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label class="text-secondary">Servicio</label>
                                                <select name="servicio_id" id="" class="form-select"
                                                    required>
                                                    <option value="">-Seleccione-</option>
                                                    @foreach ($servicios as $item)
                                                        <option value="{{ $item->id }}">{{ $item->servicio }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label class="text-secondary">Modalidad</label>
                                                <select name="modalidad_id" id="" class="form-select"
                                                    required>
                                                    <option value="">-Seleccione-</option>
                                                    @foreach ($modalidads as $item)
                                                        <option value="{{ $item->id }}">{{ $item->modalidad }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <label class="text-secondary">Comentario</label>
                                                {{-- <input type="text" class="form-control" name="comentario" required> --}}
                                                <textarea name="comentario" id="" cols="30" rows="3" class="form-control"
                                                    placeholder="Coméntanos de ti..." required></textarea>
                                            </div>
                                        </div>

                                    </div>

                                </div>

                                <div class="card-footer">
                                    <div class="row">
                                        <div class="col-12">
                                            <input type="submit" value="Registrarme"
                                                class="btn btn-success float-right">
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="py-5">
            <div class="container px-12">
                <div class="p-1 text-center bg-body-tertiary rounded-3">
                    <h1 class="text-body-emphasis">Anuncios de Empleos</h1>
                    <p class="lead">
                      {{-- Visualiza los anuncios en este lugar, verifica la fecha de publicación. --}}
                    </p>
                  </div>
                
                <div class="row gx-3 row-cols-1 row-cols-lg-4 py-5">
                    @foreach ($anuncios as $item)
                        <div class="col-sm-6 col-lg-3 mb-4" >
                            <div class="card shadow p-0 mb-0 bg-body-tertiary rounded">
                                <div class="card-header">
                                    <h6 class="card-title">{{ $item->titulo }}</h6>
                                </div>
                                <div class="card-body">
                                    <p class="card-text">{{ $item->anuncio }}</p>
                                    <footer class="blockquote-footer">
                                        publicado el {{ \Carbon\Carbon::parse($item->updated_at)->format('j/m/y') }}
                                        <a href="https://wa.me/51969987849?text=Hola, más información sobre éste anuncio.%0D%0A *{{ $item->titulo }}* %0D%0A  %0D%0A Gracias"
                                            target=".../" class="btn btn-outline-success rounded-pill px-3 btn-sm">
                                             Contactar 
                                        </a>
                                    </footer>
                                </div>
                            </div>
                        </div>
                    @endforeach

                    {{-- <div class="col">
                        <div class="feature bg-primary bg-gradient text-white rounded-3 mb-3"><i
                                class="bi bi-chat-dots"></i></div>
                        <div class="h5 mb-2">Chat with us</div>
                        <p class="text-muted mb-0">Chat live with one of our support specialists.</p>
                    </div>
                    <div class="col">
                        <div class="feature bg-primary bg-gradient text-white rounded-3 mb-3"><i
                                class="bi bi-people"></i></div>
                        <div class="h5">Ask the community</div>
                        <p class="text-muted mb-0">Explore our community forums and communicate with other users.</p>
                    </div>
                    <div class="col">
                        <div class="feature bg-primary bg-gradient text-white rounded-3 mb-3"><i
                                class="bi bi-question-circle"></i></div>
                        <div class="h5">Support center</div>
                        <p class="text-muted mb-0">Browse FAQ's and support articles to find solutions.</p>
                    </div>
                    <div class="col">
                        <div class="feature bg-primary bg-gradient text-white rounded-3 mb-3"><i
                                class="bi bi-telephone"></i></div>
                        <div class="h5">Call us</div>
                        <p class="text-muted mb-0">Call us during normal business hours at (555) 892-9403.</p>
                    </div> --}}
                </div>
            </div>
        </section>

        <!-- Testimonial section-->

        <!-- Blog preview section-->

    </main>
    <!-- Footer-->
    <footer class="bg-dark py-4 mt-auto">
        <div class="container px-5">
            <div class="row align-items-center justify-content-between flex-column flex-sm-row">
                <div class="col-auto">
                    <div class="small m-0 text-white">Copyright &copy; AgenciaHope 2023</div>
                </div>
                <div class="col-auto">
                    <a class="link-light small" href="#!">Privacidad</a>
                    <span class="text-white mx-1">&middot;</span>
                    <a class="link-light small" href="#!">Terminos</a>
                    <span class="text-white mx-1">&middot;</span>
                    <a class="link-light small" href="#!">Contactanos</a>
                </div>
            </div>
        </div>
    </footer>
    <!-- Bootstrap core JS-->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
    <!-- Core theme JS-->
    <script src="js/scripts.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="{{ asset('js/jquery.loadingModal.min.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('#receta_form_register').on('submit', function(event) {
                event.preventDefault();
                $('body').loadingModal({
                    text: 'Espere...',
                    animation: 'threeBounce',
                    opacity: '0.7',
                });
                // $('#load-on').css('display','block');
                $.ajax({
                    url: "{{ route('guardar_personas_web') }}",
                    method: "POST",
                    data: new FormData(this),
                    dataType: 'JSON',
                    contentType: false,
                    cache: false,
                    processData: false,
                    success: function(data) {
                        if (data.ok) {
                            alert(data.ok);
                            location.reload(true);
                            $('body').loadingModal('hide');

                        } else {
                            $('body').loadingModal('hide');
                            alert(data.error);
                            location.reload(true);
                        }
                    },
                    error: function(XMLHttpRequest, textStatus, errorThrown) {
                        alert("error servidor: " + XMLHttpRequest.statusText);
                        // location.reload(true);
                        $('body').loadingModal('hide');
                    }
                })
            })
        })
    </script>
</body>

</html>
