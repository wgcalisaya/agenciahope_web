<?php

use App\Http\Controllers\AnuncioController;
use App\Http\Controllers\EmpresaController;
use App\Http\Controllers\IndexController;
use App\Http\Controllers\PersonaController;
use App\Http\Controllers\RecetaController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/index', function () {
    return view('index');
});

// Auth::routes();
Route::get('login', [App\Http\Controllers\Auth\LoginController::class, 'showLoginForm'])->name('login');
Route::post('login', [App\Http\Controllers\Auth\LoginController::class, 'login']);
Route::post('logout', [App\Http\Controllers\Auth\LoginController::class, 'logout'])->name('logout');

Route::get('/', [IndexController::class,'index']);
Route::get('contactanos', [IndexController::class,'contatanos']);
Route::get('empleos', [IndexController::class,'empleos']);
Route::get('servicios', [IndexController::class,'servicios']);


Route::get('getApiServicces', [IndexController::class,'indexApi']);
Route::post('registerData', [PersonaController::class,'registrarVue']);

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('/persona/listar', [PersonaController::class,'index'])->name('lista_personas')->middleware('auth');
Route::get('/persona/create', [PersonaController::class,'create'])->name('crear_personas')->middleware('auth');
Route::post('/persona/store', [PersonaController::class,'store'])->name('guardar_personas')->middleware('auth');
Route::get('/persona/edit/{id}', [PersonaController::class,'edit'])->name('editar_personas')->middleware('auth');
Route::post('/persona/update', [PersonaController::class,'update'])->name('actualizar_personas')->middleware('auth');
Route::post('/persona/registrate', [PersonaController::class,'registrar'])->name('guardar_personas_web');

Route::get('/empresa/listar', [EmpresaController::class,'index'])->name('lista_empresa')->middleware('auth');

Route::get('/anuncio/listar', [AnuncioController::class,'index'])->name('lista_anuncios')->middleware('auth');
Route::get('/anuncio/create', [AnuncioController::class,'create'])->name('crear_anuncios')->middleware('auth');
Route::post('/anuncio/store', [AnuncioController::class,'store'])->name('guardar_anuncios')->middleware('auth');
Route::get('/anuncio/edit/{id}', [AnuncioController::class,'edit'])->name('editar_anuncios')->middleware('auth');
Route::post('/anuncio/update', [AnuncioController::class,'update'])->name('actualizar_anuncios')->middleware('auth');